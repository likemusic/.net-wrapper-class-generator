# .NET Wrapper Class Generator

This is a fork from .NET Wrapper Class Generator [http://sourceforge.net/projects/wrappergen/] with some features.

## Features
 - Create Decorator/Proxy/Wrapper class for selected classes in assembles.

## When to use?
When you are have third-party classes that not implement needed interfaces (methods/properties), and instatnce of them you cant manage.
For example, instance of Table class have Rows collection, items of that is created by Table class. If you want use data-binding to Rows, but Row class not support IBindableControl interface - you can create decorator for table class, that contatin collection of bindable rows.


