using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace WrapperGenerator.UI
{
	public partial class GeneratedWrapper : Form
	{
		#region Constructors
		internal GeneratedWrapper (string source, Language language)
		{
			InitializeComponent ();

			this.sourceTextBox.Text = source;
		}
		#endregion
	}
}