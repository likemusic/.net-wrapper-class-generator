namespace WrapperGenerator.UI
{
	partial class View
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(View));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.addAssemblyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.removeToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.languageToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.generateToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.rightSplitContainer = new System.Windows.Forms.SplitContainer();
            this.wrapperClassPropertiesPanel = new System.Windows.Forms.Panel();
            this.addInpcCheckBox = new System.Windows.Forms.CheckBox();
            this.sealedCheckBox = new System.Windows.Forms.CheckBox();
            this.partialCheckBox = new System.Windows.Forms.CheckBox();
            this.classOptionsLabel = new System.Windows.Forms.Label();
            this.classNameLabel = new System.Windows.Forms.Label();
            this.namespaceTextBox = new System.Windows.Forms.TextBox();
            this.classNameTextBox = new System.Windows.Forms.TextBox();
            this.namespaceLabel = new System.Windows.Forms.Label();
            this.propertyGrid = new System.Windows.Forms.PropertyGrid();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.splitter = new System.Windows.Forms.Splitter();
            this.assemblyBrowser = new WrapperGenerator.UI.AssemblyBrowser();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightSplitContainer)).BeginInit();
            this.rightSplitContainer.Panel1.SuspendLayout();
            this.rightSplitContainer.Panel2.SuspendLayout();
            this.rightSplitContainer.SuspendLayout();
            this.wrapperClassPropertiesPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addAssemblyToolStripButton,
            this.removeToolStripButton,
            this.toolStripSeparator1,
            this.languageToolStripComboBox,
            this.generateToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(926, 25);
            this.toolStrip.TabIndex = 6;
            // 
            // addAssemblyToolStripButton
            // 
            this.addAssemblyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("addAssemblyToolStripButton.Image")));
            this.addAssemblyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addAssemblyToolStripButton.Name = "addAssemblyToolStripButton";
            this.addAssemblyToolStripButton.Size = new System.Drawing.Size(94, 22);
            this.addAssemblyToolStripButton.Text = "Add Assembly";
            this.addAssemblyToolStripButton.ToolTipText = "Add Assembly";
            this.addAssemblyToolStripButton.Click += new System.EventHandler(this.OnAddAssemblyToolStripButtonClick);
            // 
            // removeToolStripButton
            // 
            this.removeToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("removeToolStripButton.Image")));
            this.removeToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeToolStripButton.Name = "removeToolStripButton";
            this.removeToolStripButton.Size = new System.Drawing.Size(58, 22);
            this.removeToolStripButton.Text = "Delete";
            this.removeToolStripButton.ToolTipText = "Remove Selected Type";
            this.removeToolStripButton.Click += new System.EventHandler(this.OnRemoveToolStripButtonClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // languageToolStripComboBox
            // 
            this.languageToolStripComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.languageToolStripComboBox.Items.AddRange(new object[] {
            "C#",
            "VB.NET"});
            this.languageToolStripComboBox.Name = "languageToolStripComboBox";
            this.languageToolStripComboBox.Size = new System.Drawing.Size(75, 25);
            // 
            // generateToolStripButton
            // 
            this.generateToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("generateToolStripButton.Image")));
            this.generateToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.generateToolStripButton.Name = "generateToolStripButton";
            this.generateToolStripButton.Size = new System.Drawing.Size(72, 22);
            this.generateToolStripButton.Text = "Generate";
            this.generateToolStripButton.ToolTipText = "Generate Wrapper Class";
            this.generateToolStripButton.Click += new System.EventHandler(this.OnGenerateToolStripButtonClick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Assemblies|*.dll; *.exe | (All Files) | *.*";
            this.openFileDialog.Multiselect = true;
            // 
            // rightSplitContainer
            // 
            this.rightSplitContainer.Dock = System.Windows.Forms.DockStyle.Right;
            this.rightSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.rightSplitContainer.Location = new System.Drawing.Point(695, 25);
            this.rightSplitContainer.Name = "rightSplitContainer";
            this.rightSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // rightSplitContainer.Panel1
            // 
            this.rightSplitContainer.Panel1.Controls.Add(this.wrapperClassPropertiesPanel);
            // 
            // rightSplitContainer.Panel2
            // 
            this.rightSplitContainer.Panel2.Controls.Add(this.propertyGrid);
            this.rightSplitContainer.Size = new System.Drawing.Size(231, 489);
            this.rightSplitContainer.SplitterDistance = 151;
            this.rightSplitContainer.TabIndex = 7;
            // 
            // wrapperClassPropertiesPanel
            // 
            this.wrapperClassPropertiesPanel.BackColor = System.Drawing.SystemColors.Window;
            this.wrapperClassPropertiesPanel.Controls.Add(this.addInpcCheckBox);
            this.wrapperClassPropertiesPanel.Controls.Add(this.sealedCheckBox);
            this.wrapperClassPropertiesPanel.Controls.Add(this.partialCheckBox);
            this.wrapperClassPropertiesPanel.Controls.Add(this.classOptionsLabel);
            this.wrapperClassPropertiesPanel.Controls.Add(this.classNameLabel);
            this.wrapperClassPropertiesPanel.Controls.Add(this.namespaceTextBox);
            this.wrapperClassPropertiesPanel.Controls.Add(this.classNameTextBox);
            this.wrapperClassPropertiesPanel.Controls.Add(this.namespaceLabel);
            this.wrapperClassPropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wrapperClassPropertiesPanel.Location = new System.Drawing.Point(0, 0);
            this.wrapperClassPropertiesPanel.Name = "wrapperClassPropertiesPanel";
            this.wrapperClassPropertiesPanel.Size = new System.Drawing.Size(231, 151);
            this.wrapperClassPropertiesPanel.TabIndex = 12;
            // 
            // addInpcCheckBox
            // 
            this.addInpcCheckBox.AutoSize = true;
            this.addInpcCheckBox.Location = new System.Drawing.Point(6, 124);
            this.addInpcCheckBox.Name = "addInpcCheckBox";
            this.addInpcCheckBox.Size = new System.Drawing.Size(73, 17);
            this.addInpcCheckBox.TabIndex = 13;
            this.addInpcCheckBox.Text = "Add INPC";
            this.addInpcCheckBox.UseVisualStyleBackColor = true;
            this.addInpcCheckBox.CheckedChanged += new System.EventHandler(this.OnAddInpcCheckBoxCheckedChanged);
            // 
            // sealedCheckBox
            // 
            this.sealedCheckBox.AutoSize = true;
            this.sealedCheckBox.Location = new System.Drawing.Point(67, 100);
            this.sealedCheckBox.Name = "sealedCheckBox";
            this.sealedCheckBox.Size = new System.Drawing.Size(59, 17);
            this.sealedCheckBox.TabIndex = 12;
            this.sealedCheckBox.Text = "Sealed";
            this.sealedCheckBox.UseVisualStyleBackColor = true;
            this.sealedCheckBox.CheckedChanged += new System.EventHandler(this.OnSealedCheckBoxCheckedChanged);
            // 
            // partialCheckBox
            // 
            this.partialCheckBox.AutoSize = true;
            this.partialCheckBox.Location = new System.Drawing.Point(6, 101);
            this.partialCheckBox.Name = "partialCheckBox";
            this.partialCheckBox.Size = new System.Drawing.Size(55, 17);
            this.partialCheckBox.TabIndex = 11;
            this.partialCheckBox.Text = "Partial";
            this.partialCheckBox.UseVisualStyleBackColor = true;
            this.partialCheckBox.CheckedChanged += new System.EventHandler(this.OnPartialCheckBoxCheckedChanged);
            // 
            // classOptionsLabel
            // 
            this.classOptionsLabel.AutoSize = true;
            this.classOptionsLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classOptionsLabel.Location = new System.Drawing.Point(3, 83);
            this.classOptionsLabel.Name = "classOptionsLabel";
            this.classOptionsLabel.Size = new System.Drawing.Size(102, 14);
            this.classOptionsLabel.TabIndex = 10;
            this.classOptionsLabel.Text = "Class Options:";
            // 
            // classNameLabel
            // 
            this.classNameLabel.AutoSize = true;
            this.classNameLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.classNameLabel.Location = new System.Drawing.Point(3, 43);
            this.classNameLabel.Name = "classNameLabel";
            this.classNameLabel.Size = new System.Drawing.Size(89, 14);
            this.classNameLabel.TabIndex = 9;
            this.classNameLabel.Text = "Class Name:";
            // 
            // namespaceTextBox
            // 
            this.namespaceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.namespaceTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.namespaceTextBox.Location = new System.Drawing.Point(6, 20);
            this.namespaceTextBox.Name = "namespaceTextBox";
            this.namespaceTextBox.Size = new System.Drawing.Size(222, 20);
            this.namespaceTextBox.TabIndex = 6;
            this.namespaceTextBox.Text = "MyNamespace";
            this.namespaceTextBox.TextChanged += new System.EventHandler(this.OnNamespaceTextBoxTextChanged);
            this.namespaceTextBox.Leave += new System.EventHandler(this.OnNamespaceTextBoxLeave);
            // 
            // classNameTextBox
            // 
            this.classNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.classNameTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.classNameTextBox.Location = new System.Drawing.Point(6, 60);
            this.classNameTextBox.Name = "classNameTextBox";
            this.classNameTextBox.Size = new System.Drawing.Size(222, 20);
            this.classNameTextBox.TabIndex = 8;
            this.classNameTextBox.Text = "MyWrapper";
            this.classNameTextBox.TextChanged += new System.EventHandler(this.OnClassNameTextBoxTextChanged);
            this.classNameTextBox.Leave += new System.EventHandler(this.OnClassNameTextBoxLeave);
            // 
            // namespaceLabel
            // 
            this.namespaceLabel.AutoSize = true;
            this.namespaceLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namespaceLabel.Location = new System.Drawing.Point(3, 3);
            this.namespaceLabel.Name = "namespaceLabel";
            this.namespaceLabel.Size = new System.Drawing.Size(88, 14);
            this.namespaceLabel.TabIndex = 7;
            this.namespaceLabel.Text = "Namespace:";
            // 
            // propertyGrid
            // 
            this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.Size = new System.Drawing.Size(231, 334);
            this.propertyGrid.TabIndex = 1;
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 25);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.assemblyBrowser);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.tabControl);
            this.mainSplitContainer.Size = new System.Drawing.Size(695, 489);
            this.mainSplitContainer.SplitterDistance = 271;
            this.mainSplitContainer.TabIndex = 8;
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(420, 489);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.OnTabControlSelectedIndexChanged);
            this.tabControl.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.OnTabControlControlAdded);
            this.tabControl.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.OnTabControlControlRemoved);
            // 
            // splitter
            // 
            this.splitter.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter.Location = new System.Drawing.Point(692, 25);
            this.splitter.Name = "splitter";
            this.splitter.Size = new System.Drawing.Size(3, 489);
            this.splitter.TabIndex = 9;
            this.splitter.TabStop = false;
            // 
            // assemblyBrowser
            // 
            this.assemblyBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.assemblyBrowser.ImageIndex = 0;
            this.assemblyBrowser.Location = new System.Drawing.Point(0, 0);
            this.assemblyBrowser.Name = "assemblyBrowser";
            this.assemblyBrowser.SelectedImageIndex = 0;
            this.assemblyBrowser.ShowLines = false;
            this.assemblyBrowser.Size = new System.Drawing.Size(271, 489);
            this.assemblyBrowser.TabIndex = 7;
            this.assemblyBrowser.DoubleClick += new System.EventHandler(this.OnAssemblyBrowserDoubleClick);
            this.assemblyBrowser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnAssemblyBrowserKeyDown);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 514);
            this.Controls.Add(this.splitter);
            this.Controls.Add(this.mainSplitContainer);
            this.Controls.Add(this.rightSplitContainer);
            this.Controls.Add(this.toolStrip);
            this.Name = "View";
            this.Text = ".NET Wrapper Class Generator";
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.rightSplitContainer.Panel1.ResumeLayout(false);
            this.rightSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rightSplitContainer)).EndInit();
            this.rightSplitContainer.ResumeLayout(false);
            this.wrapperClassPropertiesPanel.ResumeLayout(false);
            this.wrapperClassPropertiesPanel.PerformLayout();
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.ToolStripButton addAssemblyToolStripButton;
		private System.Windows.Forms.ToolStripButton generateToolStripButton;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripComboBox languageToolStripComboBox;
		private System.Windows.Forms.SplitContainer rightSplitContainer;
		private System.Windows.Forms.Panel wrapperClassPropertiesPanel;
		private System.Windows.Forms.CheckBox sealedCheckBox;
		private System.Windows.Forms.CheckBox partialCheckBox;
		private System.Windows.Forms.Label classOptionsLabel;
		private System.Windows.Forms.Label classNameLabel;
		private System.Windows.Forms.TextBox namespaceTextBox;
		private System.Windows.Forms.TextBox classNameTextBox;
		private System.Windows.Forms.Label namespaceLabel;
		private System.Windows.Forms.PropertyGrid propertyGrid;
		private System.Windows.Forms.SplitContainer mainSplitContainer;
		private AssemblyBrowser assemblyBrowser;
		private System.Windows.Forms.Splitter splitter;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.ToolStripButton removeToolStripButton;
        private System.Windows.Forms.CheckBox addInpcCheckBox;
	}
}

