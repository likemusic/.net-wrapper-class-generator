using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WrapperGenerator.UI
{
	internal class WrappedTypeTabPage : TabPage
	{
		#region Fields
		private WrappingOptions wrappingOptions;
		#endregion

		#region Constructors
		public WrappedTypeTabPage (WrappingOptions wrappingOptions)
		{
			this.UseVisualStyleBackColor = true;
			this.Controls.Add (wrappingOptions);
		}
		#endregion

		#region Properties
		public WrappingOptions WrappingOptions
		{
			get
			{
				return this.wrappingOptions;
			}
		}
		#endregion

		#region Protected Methods
		protected override void OnControlAdded (System.Windows.Forms.ControlEventArgs e)
		{
			if (this.Controls.Count > 1 || !(e.Control is WrappingOptions))
			{
				throw new InvalidOperationException (this.GetType ().Name + " can contain only one " + this.wrappingOptions.GetType ().Name + " and no other controls");
			}

			this.wrappingOptions = (WrappingOptions)e.Control;

			this.Text = this.wrappingOptions.WrappedType.FieldName;
			this.wrappingOptions.WrappedType.FieldNameChanged += this.OnWrappedTypeFieldNameChanged;

			base.OnControlAdded (e);

			e.Control.Dock = DockStyle.Fill;
		}		
		#endregion

		#region Private Methods
		private void OnWrappedTypeFieldNameChanged (object sender, EventArgs e)
		{
			this.Text = this.wrappingOptions.WrappedType.FieldName;
		}
		#endregion
	}
}
