using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using WrapperGenerator.ObjectModel;

namespace WrapperGenerator.UI
{
    public partial class View : Form
    {
        #region Constants
        private const string defaultNamespace = "MyNamespace";
        private const string defaultClassName = "MyWrapper";
        #endregion

        #region Fields
        private readonly WrapperClass wrapperClass;
        private readonly Dictionary<Type, TabPage> typeTabPages;
        private readonly List<string> loadedAssemblies;
        private string RuntimeDirectory; 
        
        #endregion

        #region Constructors
        public View ()
        {
            InitializeComponent ();

            this.wrapperClass = new WrapperClass ();
            this.typeTabPages = new Dictionary<Type, TabPage> ();
            this.loadedAssemblies = new List<string> ();

            //this.Size = new Size (1200, 750);

            this.languageToolStripComboBox.SelectedIndex = 0;

            RuntimeDirectory = Type.GetType("System.Object").Assembly.Location;
            string mscorlib = Environment.ExpandEnvironmentVariables(RuntimeDirectory);
            if (File.Exists (mscorlib))
            {
                this.assemblyBrowser.Assemblies.Add (mscorlib);
            }
        }
        #endregion

        #region Properties
        private Language SelectedLanguage
        {
            get
            {
                return this.languageToolStripComboBox.SelectedItem.ToString () == "C#" ? Language.CSharp : Language.VBNet;
            }
        }
        #endregion

        #region Private Methods
        private void OnAddAssemblyToolStripButtonClick (object sender, EventArgs e)
        {
            if (this.openFileDialog.ShowDialog (this) == DialogResult.OK)
            {
                this.AddAssemblies (this.openFileDialog.FileNames);
            }
        }

        private void OnGenerateToolStripButtonClick (object sender, EventArgs e)
        {
            if (this.wrapperClass.WrappedTypes.Count > 0)
            {
                this.GenerateWrapper ();
            }
            else
            {
                string message = "At least one type must be wrapped. To add a wrapped type: " + Environment.NewLine + Environment.NewLine;
                message += "1. Add an assembly (Add Assembly ToolBar button)" + Environment.NewLine;
                message += "2. Expand the assembly node and a namespace node to locate a type to be wrapped" + Environment.NewLine;
                message += "3. Double-click a type node to add it to the set of types being wrapped";
                MessageBox.Show (message, "No Types Specified", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void OnRemoveToolStripButtonClick (object sender, EventArgs e)
        {
            if (this.tabControl.SelectedTab != null)
            {
                string message = string.Format ("Are you sure you want to remove the wrapped instance '{0}' from the wrapper class?", this.tabControl.SelectedTab.Text);
                if (MessageBox.Show (message, "Confirm Remove", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    this.tabControl.TabPages.Remove (this.tabControl.SelectedTab);
                }
            }
        }

        private void OnAssemblyBrowserDoubleClick (object sender, EventArgs e)
        {
            this.AddType ();
        }

        private void OnAssemblyBrowserKeyDown (object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.AddType ();
            }
        }

        private void OnListViewSelectedIndexChanged (object sender, EventArgs e)
        {
            ListView listView = (ListView)sender;
            object[] selectedObjects = new object[listView.SelectedItems.Count];
            for (int i = 0; i < listView.SelectedItems.Count; i++)
            {
                selectedObjects[i] = listView.SelectedItems[i].Tag;
            }
            this.propertyGrid.SelectedObjects = selectedObjects;
        }

        private void OnTabControlSelectedIndexChanged (object sender, EventArgs e)
        {
            this.propertyGrid.SelectedObjects = null;
        }

        private void OnTabControlControlAdded (object sender, ControlEventArgs e)
        {
            WrappedTypeTabPage tabPage = (WrappedTypeTabPage)e.Control;
            this.wrapperClass.WrappedTypes.Add (tabPage.WrappingOptions.WrappedType);
        }

        private void OnTabControlControlRemoved (object sender, ControlEventArgs e)
        {
            WrappedTypeTabPage tabPage = (WrappedTypeTabPage)e.Control;
            this.wrapperClass.WrappedTypes.Remove (tabPage.WrappingOptions.WrappedType);
        }

        private void OnNamespaceTextBoxTextChanged (object sender, EventArgs e)
        {
            if (this.namespaceTextBox.Text == View.defaultNamespace)
            {
                this.namespaceTextBox.BackColor = SystemColors.Info;
            }
            else
            {
                this.namespaceTextBox.BackColor = SystemColors.Window;
            }
            this.wrapperClass.Namespace = this.namespaceTextBox.Text;
        }

        private void OnClassNameTextBoxTextChanged (object sender, EventArgs e)
        {
            if (this.classNameTextBox.Text == View.defaultClassName)
            {
                this.classNameTextBox.BackColor = SystemColors.Info;
            }
            else
            {
                this.classNameTextBox.BackColor = SystemColors.Window;
            }
            this.wrapperClass.ClassName = this.classNameTextBox.Text;
        }

        private void OnNamespaceTextBoxLeave (object sender, EventArgs e)
        {
            if (this.namespaceTextBox.Text == string.Empty)
            {
                this.namespaceTextBox.Text = View.defaultNamespace;
            }
        }

        private void OnClassNameTextBoxLeave (object sender, EventArgs e)
        {
            if (this.classNameTextBox.Text == string.Empty)
            {
                this.classNameTextBox.Text = View.defaultClassName;
            }
        }

        private void OnPartialCheckBoxCheckedChanged (object sender, EventArgs e)
        {
            this.wrapperClass.Partial = this.partialCheckBox.Checked;
        }

        private void OnSealedCheckBoxCheckedChanged (object sender, EventArgs e)
        {
            this.wrapperClass.Sealed = this.sealedCheckBox.Checked;
        }

        private void OnAddInpcCheckBoxCheckedChanged(object sender, EventArgs e)
        {
            this.wrapperClass.AddInpc = this.addInpcCheckBox.Checked;
        }

        private void AddAssemblies (string[] filePaths)
        {
            foreach (string filePath in filePaths)
            {
                if (this.loadedAssemblies.Contains (filePath.ToLower ()))
                {
                    MessageBox.Show (filePath + " was already loaded.", "Assembly Already Loaded", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    try
                    {
                        this.assemblyBrowser.Assemblies.Add (filePath);
                        this.loadedAssemblies.Add (filePath.ToLower ());
                        this.assemblyBrowser.Focus ();
                    }
                    catch (BadImageFormatException e)
                    {
                        MessageBox.Show(filePath + " is not a valid .NET assembly." + e.Message, "Invalid Assembly", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception exception)
                    {
                        string message = "Failed to load " + filePath + Environment.NewLine + Environment.NewLine + exception.ToString ();
                        MessageBox.Show (message, "Assembly Load Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }
        }

        private void AddType ()
        {
            if (this.assemblyBrowser.SelectedNode is TypeNode)
            {
                TypeNode typeNode = (TypeNode)this.assemblyBrowser.SelectedNode;

                // NOTE: Allow multiple instances of the same type (this is probably common)
                //if (!this.typeTabPages.ContainsKey (typeNode.Type))
                //{
                WrappedType wrappedType = new WrappedType (typeNode.Type);

                WrappingOptions wrappingOptions = new WrappingOptions (wrappedType);
                wrappingOptions.MethodsListViewSelectedIndexChanged += this.OnListViewSelectedIndexChanged;
                wrappingOptions.PropertiesListViewSelectedIndexChanged += this.OnListViewSelectedIndexChanged;
                wrappingOptions.EventsListViewSelectedIndexChanged += this.OnListViewSelectedIndexChanged;

                WrappedTypeTabPage tabPage = new WrappedTypeTabPage (wrappingOptions);
                this.tabControl.Controls.Add (tabPage);

                //this.typeTabPages[typeNode.Type] = tabPage;
                //}

                //this.tabControl.SelectedTab = this.typeTabPages[typeNode.Type];

                this.tabControl.SelectedTab = tabPage;
            }
        }

        private void GenerateWrapper ()
        {
            string source = Generator.GenerateWrapper (this.wrapperClass, this.SelectedLanguage);
            GeneratedWrapper generatedWrapper = new GeneratedWrapper (source, this.SelectedLanguage);
            generatedWrapper.ShowDialog ();
        }
        #endregion
    }
}