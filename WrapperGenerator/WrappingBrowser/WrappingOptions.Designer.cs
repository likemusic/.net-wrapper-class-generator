namespace WrapperGenerator.UI
{
	partial class WrappingOptions
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent ()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WrappingOptions));
            this.acquisitionLabel = new System.Windows.Forms.Label();
            this.acquisitionComboBox = new System.Windows.Forms.ComboBox();
            this.fieldNameLabel = new System.Windows.Forms.Label();
            this.methodsLabel = new System.Windows.Forms.Label();
            this.methodsListView = new System.Windows.Forms.ListView();
            this.methodColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.accessibilityColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.virtualColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.interfaceColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.propertiesListView = new System.Windows.Forms.ListView();
            this.propertyColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.getColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.setColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.propertyAccessibilityColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.propertyVirtualColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.interfacePropertyColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.propertiesLabel = new System.Windows.Forms.Label();
            this.eventsListView = new System.Windows.Forms.ListView();
            this.eventColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.eventAccessibilityColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.eventVirtualColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.eventInterfaceColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.eventsLabel = new System.Windows.Forms.Label();
            this.fieldNameTextBox = new System.Windows.Forms.TextBox();
            this.prefixCheckBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.MethodsCountLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.selectInvertMethodsButton = new System.Windows.Forms.Button();
            this.deselectAllMethodsButton = new System.Windows.Forms.Button();
            this.selectAllMethodsButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.PropertiesCountLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.selectInvertPropertiesButton = new System.Windows.Forms.Button();
            this.deselectAllPropertiesButton = new System.Windows.Forms.Button();
            this.selectAllPropertiesButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.selectInvertEventsButton = new System.Windows.Forms.Button();
            this.deselectAllEventsButton = new System.Windows.Forms.Button();
            this.selectAllEventsButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.EventsCountLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // acquisitionLabel
            // 
            this.acquisitionLabel.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.acquisitionLabel, 2);
            this.acquisitionLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.acquisitionLabel.Location = new System.Drawing.Point(3, 0);
            this.acquisitionLabel.Name = "acquisitionLabel";
            this.acquisitionLabel.Size = new System.Drawing.Size(184, 14);
            this.acquisitionLabel.TabIndex = 0;
            this.acquisitionLabel.Text = "Instance Acquisition Mode:";
            // 
            // acquisitionComboBox
            // 
            this.acquisitionComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.acquisitionComboBox, 2);
            this.acquisitionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.acquisitionComboBox.FormattingEnabled = true;
            this.acquisitionComboBox.Location = new System.Drawing.Point(3, 17);
            this.acquisitionComboBox.Name = "acquisitionComboBox";
            this.acquisitionComboBox.Size = new System.Drawing.Size(586, 21);
            this.acquisitionComboBox.TabIndex = 1;
            this.acquisitionComboBox.SelectedIndexChanged += new System.EventHandler(this.OnAcquisitionComboBoxSelectedIndexChanged);
            // 
            // fieldNameLabel
            // 
            this.fieldNameLabel.AutoSize = true;
            this.fieldNameLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.fieldNameLabel.Location = new System.Drawing.Point(3, 41);
            this.fieldNameLabel.Name = "fieldNameLabel";
            this.fieldNameLabel.Size = new System.Drawing.Size(86, 14);
            this.fieldNameLabel.TabIndex = 2;
            this.fieldNameLabel.Text = "Field Name:";
            // 
            // methodsLabel
            // 
            this.methodsLabel.AutoSize = true;
            this.methodsLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.methodsLabel.Location = new System.Drawing.Point(3, 0);
            this.methodsLabel.Name = "methodsLabel";
            this.methodsLabel.Size = new System.Drawing.Size(131, 14);
            this.methodsLabel.TabIndex = 4;
            this.methodsLabel.Text = "Wrapped Methods:";
            // 
            // methodsListView
            // 
            this.methodsListView.CheckBoxes = true;
            this.methodsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.methodColumnHeader,
            this.accessibilityColumnHeader,
            this.virtualColumnHeader,
            this.interfaceColumnHeader});
            this.methodsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.methodsListView.FullRowSelect = true;
            this.methodsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.methodsListView.Location = new System.Drawing.Point(3, 23);
            this.methodsListView.Name = "methodsListView";
            this.methodsListView.Size = new System.Drawing.Size(574, 127);
            this.methodsListView.SmallImageList = this.imageList;
            this.methodsListView.TabIndex = 5;
            this.methodsListView.UseCompatibleStateImageBehavior = false;
            this.methodsListView.View = System.Windows.Forms.View.Details;
            this.methodsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.OnMethodsListViewItemChecked);
            // 
            // methodColumnHeader
            // 
            this.methodColumnHeader.Text = "Method";
            this.methodColumnHeader.Width = 341;
            // 
            // accessibilityColumnHeader
            // 
            this.accessibilityColumnHeader.Text = "Accessibility";
            this.accessibilityColumnHeader.Width = 78;
            // 
            // virtualColumnHeader
            // 
            this.virtualColumnHeader.Text = "Virtual";
            // 
            // interfaceColumnHeader
            // 
            this.interfaceColumnHeader.Text = "Interface";
            this.interfaceColumnHeader.Width = 77;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Method.png");
            this.imageList.Images.SetKeyName(1, "Property.png");
            this.imageList.Images.SetKeyName(2, "Event.png");
            // 
            // propertiesListView
            // 
            this.propertiesListView.CheckBoxes = true;
            this.propertiesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.propertyColumnHeader,
            this.getColumnHeader,
            this.setColumnHeader,
            this.propertyAccessibilityColumnHeader,
            this.propertyVirtualColumnHeader,
            this.interfacePropertyColumnHeader});
            this.propertiesListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertiesListView.FullRowSelect = true;
            this.propertiesListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.propertiesListView.Location = new System.Drawing.Point(3, 23);
            this.propertiesListView.Name = "propertiesListView";
            this.propertiesListView.Size = new System.Drawing.Size(574, 127);
            this.propertiesListView.SmallImageList = this.imageList;
            this.propertiesListView.TabIndex = 7;
            this.propertiesListView.UseCompatibleStateImageBehavior = false;
            this.propertiesListView.View = System.Windows.Forms.View.Details;
            this.propertiesListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.OnPropertiesListViewItemChecked);
            // 
            // propertyColumnHeader
            // 
            this.propertyColumnHeader.Text = "Property";
            this.propertyColumnHeader.Width = 220;
            // 
            // getColumnHeader
            // 
            this.getColumnHeader.Text = "Get";
            // 
            // setColumnHeader
            // 
            this.setColumnHeader.Text = "Set";
            // 
            // propertyAccessibilityColumnHeader
            // 
            this.propertyAccessibilityColumnHeader.Text = "Accessibility";
            this.propertyAccessibilityColumnHeader.Width = 78;
            // 
            // propertyVirtualColumnHeader
            // 
            this.propertyVirtualColumnHeader.Text = "Virtual";
            // 
            // interfacePropertyColumnHeader
            // 
            this.interfacePropertyColumnHeader.Text = "Interface";
            this.interfacePropertyColumnHeader.Width = 77;
            // 
            // propertiesLabel
            // 
            this.propertiesLabel.AutoSize = true;
            this.propertiesLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.propertiesLabel.Location = new System.Drawing.Point(3, 0);
            this.propertiesLabel.Name = "propertiesLabel";
            this.propertiesLabel.Size = new System.Drawing.Size(145, 14);
            this.propertiesLabel.TabIndex = 6;
            this.propertiesLabel.Text = "Wrapped Properties:";
            // 
            // eventsListView
            // 
            this.eventsListView.CheckBoxes = true;
            this.eventsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.eventColumnHeader,
            this.eventAccessibilityColumnHeader,
            this.eventVirtualColumnHeader,
            this.eventInterfaceColumnHeader});
            this.eventsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventsListView.FullRowSelect = true;
            this.eventsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.eventsListView.Location = new System.Drawing.Point(3, 23);
            this.eventsListView.Name = "eventsListView";
            this.eventsListView.Size = new System.Drawing.Size(574, 128);
            this.eventsListView.SmallImageList = this.imageList;
            this.eventsListView.TabIndex = 9;
            this.eventsListView.UseCompatibleStateImageBehavior = false;
            this.eventsListView.View = System.Windows.Forms.View.Details;
            this.eventsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.OnEventsListViewItemChecked);
            // 
            // eventColumnHeader
            // 
            this.eventColumnHeader.Text = "Property";
            this.eventColumnHeader.Width = 341;
            // 
            // eventAccessibilityColumnHeader
            // 
            this.eventAccessibilityColumnHeader.Text = "Accessibility";
            this.eventAccessibilityColumnHeader.Width = 78;
            // 
            // eventVirtualColumnHeader
            // 
            this.eventVirtualColumnHeader.Text = "Virtual";
            // 
            // eventInterfaceColumnHeader
            // 
            this.eventInterfaceColumnHeader.Text = "Interface";
            this.eventInterfaceColumnHeader.Width = 77;
            // 
            // eventsLabel
            // 
            this.eventsLabel.AutoSize = true;
            this.eventsLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.eventsLabel.Location = new System.Drawing.Point(3, 0);
            this.eventsLabel.Name = "eventsLabel";
            this.eventsLabel.Size = new System.Drawing.Size(120, 14);
            this.eventsLabel.TabIndex = 8;
            this.eventsLabel.Text = "Wrapped Events:";
            // 
            // fieldNameTextBox
            // 
            this.fieldNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fieldNameTextBox.BackColor = System.Drawing.SystemColors.Info;
            this.fieldNameTextBox.Location = new System.Drawing.Point(3, 58);
            this.fieldNameTextBox.Name = "fieldNameTextBox";
            this.fieldNameTextBox.Size = new System.Drawing.Size(413, 20);
            this.fieldNameTextBox.TabIndex = 10;
            this.fieldNameTextBox.TextChanged += new System.EventHandler(this.OnFieldNameTextBoxTextChanged);
            this.fieldNameTextBox.Leave += new System.EventHandler(this.OnFieldNameTextBoxLeave);
            // 
            // prefixCheckBox
            // 
            this.prefixCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prefixCheckBox.AutoSize = true;
            this.prefixCheckBox.Location = new System.Drawing.Point(422, 58);
            this.prefixCheckBox.Name = "prefixCheckBox";
            this.prefixCheckBox.Size = new System.Drawing.Size(167, 17);
            this.prefixCheckBox.TabIndex = 11;
            this.prefixCheckBox.Text = "Prefix Members with Field Name";
            this.prefixCheckBox.UseVisualStyleBackColor = true;
            this.prefixCheckBox.CheckedChanged += new System.EventHandler(this.OnPrefixCheckBoxCheckedChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 173F));
            this.tableLayoutPanel1.Controls.Add(this.acquisitionComboBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.prefixCheckBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.acquisitionLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.fieldNameTextBox, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.fieldNameLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(592, 673);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 84);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(586, 586);
            this.tableLayoutPanel2.TabIndex = 12;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.methodsListView, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel9, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.Size = new System.Drawing.Size(580, 189);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.MethodsCountLabel, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.methodsLabel, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(574, 14);
            this.tableLayoutPanel6.TabIndex = 7;
            // 
            // MethodsCountLabel
            // 
            this.MethodsCountLabel.AutoSize = true;
            this.MethodsCountLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.MethodsCountLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.MethodsCountLabel.Location = new System.Drawing.Point(538, 0);
            this.MethodsCountLabel.Name = "MethodsCountLabel";
            this.MethodsCountLabel.Size = new System.Drawing.Size(33, 14);
            this.MethodsCountLabel.TabIndex = 5;
            this.MethodsCountLabel.Text = "0/0";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel9.Controls.Add(this.selectInvertMethodsButton, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.deselectAllMethodsButton, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.selectAllMethodsButton, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 156);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(312, 30);
            this.tableLayoutPanel9.TabIndex = 8;
            // 
            // selectInvertMethodsButton
            // 
            this.selectInvertMethodsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectInvertMethodsButton.Location = new System.Drawing.Point(211, 3);
            this.selectInvertMethodsButton.Name = "selectInvertMethodsButton";
            this.selectInvertMethodsButton.Size = new System.Drawing.Size(98, 24);
            this.selectInvertMethodsButton.TabIndex = 2;
            this.selectInvertMethodsButton.Text = "Invert selection";
            this.selectInvertMethodsButton.UseVisualStyleBackColor = true;
            this.selectInvertMethodsButton.Click += new System.EventHandler(this.selectInvertMethodsButton_Click);
            // 
            // deselectAllMethodsButton
            // 
            this.deselectAllMethodsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deselectAllMethodsButton.Location = new System.Drawing.Point(107, 3);
            this.deselectAllMethodsButton.Name = "deselectAllMethodsButton";
            this.deselectAllMethodsButton.Size = new System.Drawing.Size(98, 24);
            this.deselectAllMethodsButton.TabIndex = 1;
            this.deselectAllMethodsButton.Text = "Deselect all";
            this.deselectAllMethodsButton.UseVisualStyleBackColor = true;
            this.deselectAllMethodsButton.Click += new System.EventHandler(this.deselectAllMethodsButton_Click);
            // 
            // selectAllMethodsButton
            // 
            this.selectAllMethodsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectAllMethodsButton.Location = new System.Drawing.Point(3, 3);
            this.selectAllMethodsButton.Name = "selectAllMethodsButton";
            this.selectAllMethodsButton.Size = new System.Drawing.Size(98, 24);
            this.selectAllMethodsButton.TabIndex = 0;
            this.selectAllMethodsButton.Text = "Select all";
            this.selectAllMethodsButton.UseVisualStyleBackColor = true;
            this.selectAllMethodsButton.Click += new System.EventHandler(this.selectAllMethodsButton_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.propertiesListView, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel10, 0, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 198);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(580, 189);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.PropertiesCountLabel, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.propertiesLabel, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(574, 14);
            this.tableLayoutPanel7.TabIndex = 9;
            // 
            // PropertiesCountLabel
            // 
            this.PropertiesCountLabel.AutoSize = true;
            this.PropertiesCountLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.PropertiesCountLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.PropertiesCountLabel.Location = new System.Drawing.Point(538, 0);
            this.PropertiesCountLabel.Name = "PropertiesCountLabel";
            this.PropertiesCountLabel.Size = new System.Drawing.Size(33, 14);
            this.PropertiesCountLabel.TabIndex = 7;
            this.PropertiesCountLabel.Text = "0/0";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Controls.Add(this.selectInvertPropertiesButton, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.deselectAllPropertiesButton, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.selectAllPropertiesButton, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 156);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(312, 30);
            this.tableLayoutPanel10.TabIndex = 10;
            // 
            // selectInvertPropertiesButton
            // 
            this.selectInvertPropertiesButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectInvertPropertiesButton.Location = new System.Drawing.Point(211, 3);
            this.selectInvertPropertiesButton.Name = "selectInvertPropertiesButton";
            this.selectInvertPropertiesButton.Size = new System.Drawing.Size(98, 24);
            this.selectInvertPropertiesButton.TabIndex = 2;
            this.selectInvertPropertiesButton.Text = "Invert selection";
            this.selectInvertPropertiesButton.UseVisualStyleBackColor = true;
            this.selectInvertPropertiesButton.Click += new System.EventHandler(this.selectInvertPropertiesButton_Click);
            // 
            // deselectAllPropertiesButton
            // 
            this.deselectAllPropertiesButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deselectAllPropertiesButton.Location = new System.Drawing.Point(107, 3);
            this.deselectAllPropertiesButton.Name = "deselectAllPropertiesButton";
            this.deselectAllPropertiesButton.Size = new System.Drawing.Size(98, 24);
            this.deselectAllPropertiesButton.TabIndex = 1;
            this.deselectAllPropertiesButton.Text = "Deselect all";
            this.deselectAllPropertiesButton.UseVisualStyleBackColor = true;
            this.deselectAllPropertiesButton.Click += new System.EventHandler(this.deselectAllPropertiesButton_Click);
            // 
            // selectAllPropertiesButton
            // 
            this.selectAllPropertiesButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectAllPropertiesButton.Location = new System.Drawing.Point(3, 3);
            this.selectAllPropertiesButton.Name = "selectAllPropertiesButton";
            this.selectAllPropertiesButton.Size = new System.Drawing.Size(98, 24);
            this.selectAllPropertiesButton.TabIndex = 0;
            this.selectAllPropertiesButton.Text = "Select all";
            this.selectAllPropertiesButton.UseVisualStyleBackColor = true;
            this.selectAllPropertiesButton.Click += new System.EventHandler(this.selectAllPropertiesButton_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel11, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.eventsListView, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 393);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(580, 190);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.Controls.Add(this.selectInvertEventsButton, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.deselectAllEventsButton, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.selectAllEventsButton, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 157);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(312, 30);
            this.tableLayoutPanel11.TabIndex = 11;
            // 
            // selectInvertEventsButton
            // 
            this.selectInvertEventsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectInvertEventsButton.Location = new System.Drawing.Point(211, 3);
            this.selectInvertEventsButton.Name = "selectInvertEventsButton";
            this.selectInvertEventsButton.Size = new System.Drawing.Size(98, 24);
            this.selectInvertEventsButton.TabIndex = 2;
            this.selectInvertEventsButton.Text = "Invert selection";
            this.selectInvertEventsButton.UseVisualStyleBackColor = true;
            this.selectInvertEventsButton.Click += new System.EventHandler(this.selectInvertEventsButton_Click);
            // 
            // deselectAllEventsButton
            // 
            this.deselectAllEventsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deselectAllEventsButton.Location = new System.Drawing.Point(107, 3);
            this.deselectAllEventsButton.Name = "deselectAllEventsButton";
            this.deselectAllEventsButton.Size = new System.Drawing.Size(98, 24);
            this.deselectAllEventsButton.TabIndex = 1;
            this.deselectAllEventsButton.Text = "Deselect all";
            this.deselectAllEventsButton.UseVisualStyleBackColor = true;
            this.deselectAllEventsButton.Click += new System.EventHandler(this.deselectAllEventsButton_Click);
            // 
            // selectAllEventsButton
            // 
            this.selectAllEventsButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectAllEventsButton.Location = new System.Drawing.Point(3, 3);
            this.selectAllEventsButton.Name = "selectAllEventsButton";
            this.selectAllEventsButton.Size = new System.Drawing.Size(98, 24);
            this.selectAllEventsButton.TabIndex = 0;
            this.selectAllEventsButton.Text = "Select all";
            this.selectAllEventsButton.UseVisualStyleBackColor = true;
            this.selectAllEventsButton.Click += new System.EventHandler(this.selectAllEventsButton_Click);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.EventsCountLabel, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.eventsLabel, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(574, 14);
            this.tableLayoutPanel8.TabIndex = 10;
            // 
            // EventsCountLabel
            // 
            this.EventsCountLabel.AutoSize = true;
            this.EventsCountLabel.Dock = System.Windows.Forms.DockStyle.Right;
            this.EventsCountLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold);
            this.EventsCountLabel.Location = new System.Drawing.Point(538, 0);
            this.EventsCountLabel.Name = "EventsCountLabel";
            this.EventsCountLabel.Size = new System.Drawing.Size(33, 14);
            this.EventsCountLabel.TabIndex = 9;
            this.EventsCountLabel.Text = "0/0";
            // 
            // WrappingOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "WrappingOptions";
            this.Size = new System.Drawing.Size(592, 673);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label acquisitionLabel;
		private System.Windows.Forms.ComboBox acquisitionComboBox;
		private System.Windows.Forms.Label fieldNameLabel;
		private System.Windows.Forms.Label methodsLabel;
		private System.Windows.Forms.ListView methodsListView;
		private System.Windows.Forms.ColumnHeader methodColumnHeader;
		private System.Windows.Forms.ImageList imageList;
		private System.Windows.Forms.ColumnHeader accessibilityColumnHeader;
		private System.Windows.Forms.ColumnHeader virtualColumnHeader;
		private System.Windows.Forms.ColumnHeader interfaceColumnHeader;
		private System.Windows.Forms.ListView propertiesListView;
		private System.Windows.Forms.ColumnHeader propertyColumnHeader;
		private System.Windows.Forms.ColumnHeader propertyAccessibilityColumnHeader;
		private System.Windows.Forms.ColumnHeader propertyVirtualColumnHeader;
		private System.Windows.Forms.ColumnHeader interfacePropertyColumnHeader;
		private System.Windows.Forms.Label propertiesLabel;
		private System.Windows.Forms.ColumnHeader getColumnHeader;
		private System.Windows.Forms.ColumnHeader setColumnHeader;
		private System.Windows.Forms.ListView eventsListView;
		private System.Windows.Forms.ColumnHeader eventColumnHeader;
		private System.Windows.Forms.ColumnHeader eventAccessibilityColumnHeader;
		private System.Windows.Forms.ColumnHeader eventVirtualColumnHeader;
		private System.Windows.Forms.ColumnHeader eventInterfaceColumnHeader;
		private System.Windows.Forms.Label eventsLabel;
		private System.Windows.Forms.TextBox fieldNameTextBox;
		private System.Windows.Forms.CheckBox prefixCheckBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label MethodsCountLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label PropertiesCountLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label EventsCountLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Button selectInvertMethodsButton;
        private System.Windows.Forms.Button deselectAllMethodsButton;
        private System.Windows.Forms.Button selectAllMethodsButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Button selectInvertPropertiesButton;
        private System.Windows.Forms.Button deselectAllPropertiesButton;
        private System.Windows.Forms.Button selectAllPropertiesButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Button selectInvertEventsButton;
        private System.Windows.Forms.Button deselectAllEventsButton;
        private System.Windows.Forms.Button selectAllEventsButton;
	}
}
